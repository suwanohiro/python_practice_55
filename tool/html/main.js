window.onload = function () {
    addEvent.onload();
    MainFunction.main();
}

class addEvent {
    static onload() {
        var input = document.getElementsByClassName("input");

        input[0].addEventListener("change", function () {
            MainFunction.main();
        });

        input[1].addEventListener("change", function () {
            var input = document.getElementsByClassName("input");
            var range_num = document.getElementsByClassName("range_num");
            range_num[0].innerHTML = Shaping.FillZero(input[1].value, 2);
            MainFunction.main();
        });

        input[2].addEventListener("change", function () {
            var input = document.getElementsByClassName("input");
            var range_num = document.getElementsByClassName("range_num");
            range_num[1].innerHTML = Shaping.FillZero(input[2].value, 2);
            MainFunction.main();
        });

        input[3].addEventListener("change", function () {
            MainFunction.main();
        });
    }
}

class Shaping {
    static FillZero(num1, num2) {
        var str = "";
        for (var a = 0; a < num2; a++) {
            str += "0";
        }
        return (str + num1).slice(num2 * -1);
    }
}

class MainFunction {
    static main() {
        var input = document.getElementsByClassName("input");
        var input_Text = String(input[3].value).split(/\r\n|\n/);
        var output = document.getElementById("output");
        var FirstLineFlg = input[0].checked;
        var result = new Array(input_Text.length).fill("");

        output.value = "";

        for (var a = 0; a < input_Text.length; a++) {
            var Indent = "";
            if (!(!FirstLineFlg && a == 0)) {
                Indent = this.MakeIndent(Number(input[1].value) * Number(input[2].value));
            }
            result[a] = Indent + String(input_Text[a]) + "\n";
        }
        for (var a = 0; a < input_Text.length; a++) {
            output.value += result[a];
        }
        console.log(result);
    }

    static MakeIndent(num) {
        var result = "";
        var str = "";
        for (var a = 0; a < num; a++) {
            str += " ";
        }
        result = str;
        return result;
    }
}