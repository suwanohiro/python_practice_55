import PySimpleGUI as sg

# 画面構成
layout = [
    [sg.Checkbox("1行目を処理する", default=False), sg.Text(size=(10, 1)), sg.Text("作成インデント数："), sg.Slider(
        range=(1.0, 5.0), default_value=1.0, orientation='h')],
    [sg.Button("実行", size=(960, 3))],
    [sg.Text("入力", size=(59, 1)), sg.Text("処理結果", size=(65, 1))],
    [sg.Multiline(key="main", size=(65, 960)),
     sg.Multiline(key="result", size=(65, 960))],
]

# ウィンドウの生成
window = sg.Window("Pythonインデント補正", layout, size=(960, 540))

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "test":
        print("testボタンが押されました。")
    elif event == "asd":
        print("keyがasdのボタンが押されました。")

window.close()
