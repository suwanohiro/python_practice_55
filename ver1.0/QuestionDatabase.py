from multiprocessing.dummy import Array
import tools as tools


class Cash:
    CashData = ["", "", "", "", ""]

    def SetCashData(data, num):
        Cash.CashData[num] = data

    def GetCashData(num):
        return Cash.CashData[num]

    def ResetCashData():
        a = 0
        while a < len(Cash.CashData):
            Cash.CashData[a] = ""
            a = a + 1


def Q01():
    x = 2
    return tools.innerDisplay(3 * x)


def Q02():
    a = 100
    b = 200
    a, b = b, a
    return tools.innerDisplay(f"{a}, {b}")


def Q03():
    a = 10
    b = 2
    return tools.innerDisplay(f"{a+b}, {a-b}, {a*b}, {a/b}")


def Q04():
    a = 5
    b = 3
    return tools.innerDisplay(a % b)


def Q05():
    a = 5
    b = 10
    return tools.innerDisplay(a**b)


def Q06():
    a = 5
    b = 10
    if a < b:
        return tools.innerDisplay(b)
    else:
        return tools.innerDisplay(a)


def Q07():
    a = 5
    if a % 2 == 0:
        return tools.innerDisplay(True)
    else:
        return tools.innerDisplay(False)


def Q08():
    s = 'python'
    return tools.innerDisplay(s[2])


def Q09():
    s1 = 'py'
    s2 = 'thon'
    return tools.innerDisplay(s1 + s2)


def Q10():
    a = 5
    b = 3
    ans = "{}%{}={}".format(a, b, a % b)
    return tools.innerDisplay(ans)


def Q11():
    s = 'some1'
    s = s.replace('1', 'one')
    return tools.innerDisplay(s)


def Q12():
    s = 'This Is A Sentence .'
    s = s.lower()
    return tools.innerDisplay(s)


def Q13():
    s = 'This Is A Sentence .'
    s = s.upper()
    return tools.innerDisplay(s)


def Q14():
    s = 'How many characters?'
    return tools.innerDisplay(len(s))


def Q15():
    s1 = '34'
    s2 = '43'
    return tools.innerDisplay(int(s1) + int(s2))


def Q16():
    li = [1, 2, 3, 4, 5]
    return tools.innerDisplay(li[3])


def Q17():
    li1 = [1, 2, 3]
    li2 = [4, 5]
    return tools.innerDisplay(li1 + li2)


def Q18():
    li = [1, 2, 3, 4, 5]
    li.append(6)
    li.append(7)
    return tools.innerDisplay(li)


def Q19():
    li = [1, 2, 3, 4, 5]
    li.insert(1, 100)
    return tools.innerDisplay(li)


def Q20():
    li = [1, 2, 3, 4, 5]
    string = ""
    for elem in li:
        if elem % 2 == 0:
            string += str(elem)
    return tools.innerDisplay(string)


def Q21():
    li = [1, 2, 3, 4, 5]
    string = ""
    for idx, elem in enumerate(li):
        if idx % 2 == 0:
            string += str(elem)
    return tools.innerDisplay(string)


def Q22():
    li = [11, 22, 33, 44, 55, 66]
    return tools.innerDisplay(len(li))


def Q23():
    li = [11, 22, 33, 44, 55, 66]
    if 44 in li:
        return tools.innerDisplay(True)
    else:
        return tools.innerDisplay(False)


def Q24():
    li = [1, 2, 3, 4, 5]
    tup = (li[0], li[-1])
    return tools.innerDisplay(tup)


def Q25():
    d = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5}
    return tools.innerDisplay(d)


def Q26():
    d = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5}
    return tools.innerDisplay(list(d.keys()))


def Q27():
    d = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5}
    return tools.innerDisplay(list(d.values()))


def Q28():
    d = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5}
    return tools.innerDisplay(list(d.items()))


def Q29():
    d = {'apple': 10, 'grape': 20, 'orange': 30, 'pineapple': -1}
    d['apple'] = d.get('apple', -1)
    d['pineapple'] = d.get('pineapple', -1)
    return tools.innerDisplay(d)


def Q30():
    s = 'training'
    return tools.innerDisplay(s[1:5])


def Q31():
    s = 'understand'
    return tools.innerDisplay(s[1::2])


def Q32():
    li = [1, 2, 3, 4, 5]
    li = li[::-1]
    return tools.innerDisplay(li)


def Q33():
    li = [1, 1, 2, 3, 3, 4, 5]
    se = set(li)
    return tools.innerDisplay(se)


def Q34():
    set1 = {1, 2, 3, 4, 5}
    set2 = {3, 4, 5, 6, 7}
    return tools.innerDisplay(set1 & set2)


def Q35():
    set1 = {1, 2, 3, 4, 5}
    set2 = {3, 4, 5, 6, 7}
    return tools.innerDisplay(set1 | set2)


def Q36():
    set1 = {1, 2, 3, 4, 5}
    set2 = {3, 4, 5, 6, 7}
    return tools.innerDisplay(set1 - set2)


def Q37():
    data1 = {'A': 1, 'B': 2}
    data2 = "hoge"
    data3 = {1, 2, 3, 4, 5}
    return tools.innerDisplay(f"{type(data1)}, {type(data2)}, {type(data3)}")


def Q38():
    s = 'This is sentence .\n'
    return tools.innerDisplay(s.rstrip())


def Q39():
    s = 'C C++ // python java'
    return tools.innerDisplay(f"{s.split()}, {s.split('/')}")


def Q40():
    words = ['This', 'is', 'a', 'sentence']
    sent = ' '.join(words)
    return tools.innerDisplay(sent)


def Q41():
    li = [11, 2, 7, 13, 5]
    return tools.innerDisplay(max(li))


def Q42():
    li = [11, 2, 7, 13, 5]
    return tools.innerDisplay(min(li))


def Q43():
    li = [11, 2, 7, 13, 5]
    return tools.innerDisplay(sum(li))


def Q44():
    li = [5, 3, 1, 4, 2]
    sorted_li = sorted(li)
    return tools.innerDisplay(sorted_li)


def Q45():
    li = [{'a': 6, 'b': 7, 'c': 6},
          {'a': 4, 'b': 2, 'c': 3},
          {'a': 1, 'b': 5, 'c': 8}]
    sorted_li = sorted(li, reverse=True, key=lambda x: x['b'])
    return tools.innerDisplay(sorted_li)


def Q46():
    li = list(range(0, 100))
    return tools.innerDisplay(li)


def Q47():
    li = [5, 4, 3, 2, 1]
    ans_li = [idx + elem for idx, elem in enumerate(li)]
    return tools.innerDisplay(ans_li)


def Q48():
    a = 0
    b = 5
    string = ""
    try:
        Cash.SetCashData(str(a / b), 0)
    except ZeroDivisionError:
        Cash.SetCashData('zero division', 0)

    try:
        Cash.SetCashData(str(b / a), 1)
    except ZeroDivisionError:
        Cash.SetCashData('zero division', 1)
    string = f"{Cash.GetCashData(0)}, {Cash.GetCashData(1)}"
    Cash.ResetCashData()
    return tools.innerDisplay(string)


def Q49():
    a = 10
    b = 5
    return tools.innerDisplay(f"{a | b}, {a & b}, {a ^ b}")


def Q50():
    import math
    theta = math.pi / 2
    ans = math.sin(theta)**2 + math.cos(theta)**2
    return tools.innerDisplay(ans)


def Q51():
    li1 = list(range(1, 32))
    li2 = list(range(1, 13))
    counter = 0
    for elem1 in li1:
        for elem2 in li2:
            if elem1 % 10 == elem2 % 10:
                counter += 1
    return tools.innerDisplay(counter)


def Q52():
    dic = {'two': 324, 'four': 830, 'three': 493, 'one': 172, 'five': 1024}
    items_list = list(dic.items())
    sorted_items_list = sorted(items_list, key=lambda x: x[1])
    ans = [elem[0] for elem in sorted_items_list]
    return tools.innerDisplay(ans)


def Q53():
    nums = [1, 2, 4, 3, 2, 1, 5, 1]
    num2freq = {}
    for num in nums:
        num2freq[num] = num2freq.get(num, 0) + 1

    return tools.innerDisplay(num2freq)


def Q54():
    doc = 'i bought an apple .\ni ate it .\nit is delicious .'
    word2freq = {}
    sents = doc.split('\n')
    for sent in sents:
        words = sent.split()
        for word in words:
            word2freq[word] = word2freq.get(word, 0) + 1

    return tools.innerDisplay(word2freq)


def Q55():
    list1 = [12, 23, 34, 45, 56, 67, 78, 89]
    list2 = [21, 32, 43, 45, 65, 67, 78, 98]

    A = set(list1)
    B = set(list2)
    inter_set = A.intersection(B)
    union_set = A.union(B)
    return tools.innerDisplay(len(inter_set) / len(union_set))


QuestionFunction = [Q01, Q02, Q03, Q04, Q05,
                    Q06, Q07, Q08, Q09, Q10, Q11, Q12, Q13, Q14, Q15, Q16, Q17, Q18, Q19, Q20, Q21, Q22, Q23, Q24, Q25, Q26, Q27, Q28, Q29, Q30, Q31, Q32, Q33, Q34, Q35, Q36, Q37, Q38, Q39, Q40, Q41, Q42, Q43, Q44, Q45, Q46, Q47, Q48, Q49, Q50, Q51, Q52, Q53, Q54, Q55]


def Question(num):
    return QuestionFunction[num - 1]()
