import PySimpleGUI as sg
import QuestionDatabase as qd


class NumberChange:
    def Change(num):
        result = str(num)
        if num < 10:
            result = f"0{num}"
        return result


class FileAction:
    import os

    def ConvertFileLink(str):
        data = FileAction.os
        base = data.path.dirname(data.path.abspath(__file__))
        name = data.path.normpath(data.path.join(base, str))
        return name

    def Read(str):
        path = FileAction.ConvertFileLink(str)
        Files = open(path, "r", encoding="UTF-8")
        return Files.read()


class Question:
    now = 1
    max = len(qd.QuestionFunction)
    min = 1

    QuestionResultID = "QRID"

    def Change(num):
        Question.now = num
        Question.QuestionNumber(num)
        Question.QuestionResult(num)
        code = Question.QuestionSourceCode(num)
        Question.QuestionSourceCodeUpdate(code)

    def QuestionNumber(num):
        window["qnum"].update(
            f"問題番号 {NumberChange.Change(num)} / {Question.max}")

    def QuestionResult(num):
        window[Question.QuestionResultID].update(qd.Question(num))

    def QuestionSourceCode(num):
        path = f"program/Q{NumberChange.Change(num)}.py"
        FileData = FileAction.Read(path)
        return FileData

    def QuestionSourceCodeUpdate(FileData):
        window["program"].update(FileData)


# 画面構成
layout = [
    [sg.Button(" 前の問題 ", key="back"), sg.Button(" 次の問題 ", key="next"), sg.Text(
        size=(5, 1)), sg.InputText(size=(10, 1), key="input"), sg.Button("指定した問題番号に移動", key="MoveNum")],
    [sg.Text(f"問題番号 01 / {Question.max}", key="qnum")],
    [sg.Text()],
    [sg.Text("処理結果："), sg.InputText(
        qd.Question(1), key="QRID", size=(500, 1))],
    [sg.Text("ソースコード")],
    [sg.Multiline(Question.QuestionSourceCode(
        1), size=(1260, 620), key="program")]
]

# ウィンドウの生成
window = sg.Window("練習問題55本ノック", layout, size=(960, 540))

# イベントループ
while True:
    event, value = window.read()
    now = Question.now

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "back":
        target = now - 1
        if now == Question.min:
            target = Question.max
        Question.Change(target)
    elif event == "next":
        target = now + 1
        if now == Question.max:
            target = Question.min
        Question.Change(target)
    elif event == "MoveNum":
        input = value["input"]
        if input.isascii() == True and input.isalnum() == True and Question.min <= int(input) <= Question.max:
            Question.Change(int(input))
        else:
            sg.popup(
                f"{Question.min}から{Question.max}の間の数を半角数字で入力してください。", title="エラー")
        window["input"].update("")

window.close()
