a = 5
if a % 2 == 0:
    print(True)
else:
    print(False)
# もしくは
print(a % 2 == 0)
