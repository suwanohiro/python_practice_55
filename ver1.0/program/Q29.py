d = {'apple': 10, 'grape': 20, 'orange': 30, 'pineapple': -1}
d['apple'] = d.get('apple', -1)
d['pineapple'] = d.get('pineapple', -1)
print(d)
