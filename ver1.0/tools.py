class FileAction:
    import os
    CashDataFile = "CashData.ini"
    CashData = ""

    def ConvertFileLink():
        data = FileAction.os
        base = data.path.dirname(data.path.abspath(__file__))
        name = data.path.normpath(
            data.path.join(base, FileAction.CashDataFile))
        return name

    def Read():
        path = FileAction.ConvertFileLink()
        Files = open(path, "r", encoding="UTF-8")
        result = Files.read()
        Files.close()
        return result

    def Write(str):
        path = FileAction.ConvertFileLink()
        Files = open(path, "w", encoding="UTF-8")
        print(str, file=Files, end="")
        Files.close()


def innerDisplay(str):
    FileAction.Write(str)
    result = FileAction.Read()
    # result = FileAction.CashData
    return result
